'use strict'
const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
	product_code: { type:String, required: true },
	has_selected:{ type:Boolean, default: false },
	max_scan: {type: Number },
	register_date:{ type:Date },
	prospects: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ScannProspect' }],
	selected: { type: mongoose.Schema.Types.ObjectId, ref: 'ScannProspect' }
});
module.exports.Product = ProductSchema;

const ScannProspectchema = new mongoose.Schema({
	address: { type:String },
	product_code: { type:String, required: true },
	register_date:{ type:Date  },
	author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	data: { type: Object, default:{
		code: { type:String },
		company_name: { type:String },
		name_product: { type:String },
		weight: { type:String },
		serving_size: { type:String },
		calorias: { type:String },
		total_fat: { type:String },
		saturated_fat: { type:String },
		cholesterol: { type:String },
		sodiom: { type:String },
		total_carbohydrate: { type:String },
		dietary_fiber: { type:String },
		protein: { type:String },
		ingredients: { type:String },
		sugar: { type:String },
	}
	}
});
module.exports.ScannProspect = ScannProspectchema;

