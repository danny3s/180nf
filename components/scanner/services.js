const Product = require('./model').Product,
      ScannProspect = require('./model').ScannProspect,
      User = require('../auth/model').User,
      service = {},
      { MAX_SCANNER_PER_PRODUCT } = process.env,
      moment = require('moment'),
      connectToDatabase = require('../../config/db-connection');
      

service.checkForProduct = async(product_code) => {
  let connection = await connectToDatabase();
  let model = connection.model('Product',Product);
  return new Promise((resolve, reject) => {
    model.findOne({product_code})
    .populate({path:"prospects", model: connection.model('ScannProspect',ScannProspect),
      populate:{
        path:"author", model: connection.model('User',User),
        select: "-isactive -was_surveyed -made_the_first_charge -pin -password -register_date -activationcode -wallet_address -wallet_key"
      }
    })
    .exec(function(err, data) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
        if(!data){
          return reject({message:"Product no encontrado", code:404});
        } else{
          return resolve(data);
        }
      };
    });
  });
};

service.NewProduct = async(data, userId) => {
  let connection = await connectToDatabase();
  let productModel = connection.model('Product',Product);
  let prospectModel = connection.model('ScannProspect',ScannProspect);
  let prospect = new prospectModel(data);
  prospect.product_code = data.data.code;
  prospect.register_date = moment().utc();
  prospect.author = userId;
  let product = new productModel({
    product_code: data.data.code,
    max_scan: MAX_SCANNER_PER_PRODUCT,
    register_date: moment().utc(),
    prospects: []
  });

  return new Promise((resolve, reject) => {
    productModel.findOne({product_code:data.data.code.toString()}, function(err, finded) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
        if(!finded){
          prospect.save(function(err, prspectCreated) {
            if (err) {
                return reject({
                    message: "Ocurrio un error al hacer la peticion",
                    error:err
                });
            } else {
              product.prospects.push(prspectCreated._id)
              product.save(function(err, productCreated) {
                if (err) {
                    return reject({
                        message: "Ocurrio un error al hacer la peticion",
                        error:err
                    });
                } else {
                  return resolve(productCreated);
                };
              });
            };
          });
        } else{
          if(finded.prospects && finded.prospects.length > 9){
            return resolve({message:"Cuota alcanzada de prospectos",finded});
          }
          else{
            prospectModel.find({product_code:data.data.code, author:userId},function(err, prospectFinded) {
              if (err) {
                  return reject({
                      message: "Ocurrio un error al hacer la peticion",
                      error:err
                  });
              } else {
                if(!prospectFinded || !prospectFinded.length){
                 prospect.save(function(err, prospectCreatedForUpdate) {
                  if (err) {
                      return reject({
                          message: "Ocurrio un error al hacer la peticion",
                          error:err
                      });
                  } else {
                    productModel.findOneAndUpdate({product_code:data.data.code}, {$push: {prospects: prospectCreatedForUpdate._id}}, {new:true},function(err, productUpdated) {
                      if (err) {
                          return reject({
                              message: "Ocurrio un error al hacer la peticion",
                              error:err
                          });
                      } else {
                        return resolve({message:"Prospecto agregado", productUpdated});
                      };
                    });
                  };
                });
                }else {
                  return resolve({message:"Ya tienes un prospecto para este producto", prospectFinded, code: 403})
                }
              };
            });
          }
        }
      };
    });
  });
};



service.getProducts = async(perPage, page) => {
  let connection = await connectToDatabase();
  let model = connection.model('Product',Product);
  return new Promise((resolve, reject) => {
    model.find({})
    .skip(parseInt(perPage) * parseInt(page))
    .limit(parseInt(perPage))
    .populate({path:"prospects", 
    model: connection.model('ScannProspect',ScannProspect), 
    populate: {path:"author", 
    model: connection.model('User',User), 
    select: "-isactive -was_surveyed -made_the_first_charge -pin -password -register_date -activationcode -wallet_address -wallet_key",
  }})
    .exec(function(err, data) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
        if(!data){
          return reject({message:"Product no encontrado", code:404});
        } else{
          return resolve(data);
        }
      };
    });
  });
};


service.getMyParticipations = async(userId,perPage, page) => {
  let connection = await connectToDatabase();
  let scannerProspect = connection.model('ScannProspect',ScannProspect);
  return new Promise((resolve, reject) => {
    scannerProspect.find({author: userId})
    .skip(parseInt(perPage) * parseInt(page))
    .limit(parseInt(perPage))
    .populate({path:"author", model: connection.model('User',User), select: "-isactive -was_surveyed -made_the_first_charge -pin -password -register_date -activationcode -wallet_address -wallet_key",})
    .exec(function(err, data) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
        if(!data){
          return reject({message:"Productos no encontrados", code:404});
        } else{
          return resolve(data);
        }
      };
    });
  });
};


service.getVerifiedProducts = async(perPage, page) => {
  let connection = await connectToDatabase();
  let scannerProspect = connection.model('Product',Product);
  return new Promise((resolve, reject) => {
    scannerProspect.find({has_selected:true})
    .skip(parseInt(perPage) * parseInt(page))
    .limit(parseInt(perPage))
    .populate({path:"prospects", 
    model: connection.model('ScannProspect',ScannProspect), 
    populate: {path:"author", 
    model: connection.model('User',User), 
    select: "-isactive -was_surveyed -made_the_first_charge -pin -password -register_date -activationcode -wallet_address -wallet_key",
  }})
    .exec(function(err, data) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
        if(!data){
          return reject({message:"Productos no encontrados", code:404});
        } else{
          return resolve(data);
        }
      };
    });
  });
};

service.updateProduct = async(id, data) => {
  let connection = await connectToDatabase();
  let scannerProspect = connection.model('Product',Product);
  return new Promise((resolve, reject) => {
    scannerProspect.findOneAndUpdate({_id:id}, data,{new:true}, function(err, updated) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
          return resolve(updated);
      };
    });
  });
};

service.getProductByCode = async(code) => {
  let connection = await connectToDatabase();
  let scannerProspect = connection.model('Product',Product);
  return new Promise((resolve, reject) => {
    scannerProspect.findOne({product_code:code})
    .populate({path:"prospects", model: connection.model('ScannProspect',ScannProspect),
      populate:{
        path:"author", model: connection.model('User',User),
        select: "-isactive -was_surveyed -made_the_first_charge -pin -password -register_date -activationcode -wallet_address -wallet_key",
      }
    })
    .exec(function(err, updated) {
      if (err) {
          return reject({
              message: "Ocurrio un error al hacer la peticion",
              error:err
          });
      } else {
          return resolve(updated);
      };
    });
  });
};

module.exports = service;

