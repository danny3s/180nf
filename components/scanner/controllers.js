const Services = require('./services');

exports.checkForProduct = function(req,res,next){
	const { params: {product_code} }= req;
	Services.checkForProduct(product_code.toString())
		.then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.send(err);
		});
};

exports.NewProduct = function(req,res,next){
	const { body, user: { sub } } = req;
	Services.NewProduct(body, sub)
		.then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.send(err);
		});
};

 
exports.getProducts = function(req,res,next){
const { params: {perPage, page} } = req;
Services.getProducts(perPage, page)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	});
};

exports.getMyParticipations = function(req,res,next){
	const { params: {perPage, page}, user: { sub } } = req;
	Services.getMyParticipations(sub, perPage, page)
		.then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.send(err);
		});
};

exports.getVerifiedProducts = function(req,res,next){
	const { params: {perPage, page} } = req;
	Services.getVerifiedProducts(perPage, page)
		.then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.send(err);
		});
};

exports.updateProduct = function(req,res,next){
	const { body, params: {id} } = req;
	Services.updateProduct(id, body)
		.then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.send(err);
		});
};

exports.getProductByCode = function(req,res,next){
	const { params: {code} } = req;
	Services.getProductByCode(code)
		.then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.send(err);
		});
};