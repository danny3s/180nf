'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubscriptorSchema =  Schema({
	subscriptor_email:{ type:String, required:true },
	subscription_date:{ type:Date, required:true }
});
try {
	module.exports.Subscriptor = mongoose.model('Subscriptor');
} catch (e) {
	const Subscriptor = mongoose.model('Subscriptor',SubscriptorSchema);
module.exports.Subscriptor = Subscriptor;
}

