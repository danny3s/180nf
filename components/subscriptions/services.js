var Subscriptor = require('./model').Subscriptor;
var service = {};
var moment = require('moment');

service.subscribe = function(data){
    return new Promise(function(resolve, reject){
      const subscriptor = new Subscriptor(data);
      subscriptor.subscription_date = moment.now();
      Subscriptor.findOne({subscriptor_email: data.subscriptor_email}, function(errsubscriptor,subscriptorFound){
        if(errsubscriptor){
          return reject(errsubscriptor);
        }
        else{
          if(!subscriptorFound){
            Subscriptor.create(subscriptor, function(err, created) {
              if (err) {
                   return reject(err);
              }
              else{
                return resolve(created);
              }
            });
          }
          else{
            return reject({message: "Subscriptor ya realizada"})
          }
        }
      });     
    })
}

service.allsubscriptors = function(){
  return new Promise(function(resolve, reject){
    Subscriptor.find({}, function(errsubscriptor,subscriptors){
      if(errsubscriptor){
        return reject(errsubscriptor);
      }
      else{
        return resolve(subscriptors);
      }
    });     
  })
}


module.exports = service;

