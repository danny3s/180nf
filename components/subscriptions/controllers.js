var Services = require('./services');

exports.subscribe = function(req,res,next){
	const {body} = req;
	Services.subscribe(body)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.allsubscriptors = function(req,res,next){
	Services.allsubscriptors()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};