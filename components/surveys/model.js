'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SurveySchema =  Schema({
	questions: {
		type: Array,
		require: true, 
		default:[
			{
				question:{ type:String, required:true },
				response:{ type:String, required:true }
			}
		]
		},
	enquest_date:{ type:Date, required:true } 	
});
	try {
		module.exports.Survey = mongoose.model('Survey');
	} catch (e) {
		const Survey = mongoose.model('Survey',SurveySchema);
		module.exports.Survey = Survey;
	}
  



