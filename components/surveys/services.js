const Survey = require('./model').Survey,
    service = {},
    moment = require('moment');

service.newSurvey =  function(data){
  return new Promise(function(resolve, reject){  
    const now = moment.now();
    const survey = new Survey({
      questions: JSON.parse(data),
      enquest_date: now
    });
    Survey.create(survey, (err, created) =>{
      if(err){
        return reject(err);
      }
      else{
        return resolve(created);
      }
    })
});
}

service.getAll =  function(){
  return new Promise(function(resolve, reject){  
    Survey.find({}, (err, finded) =>{
      if(err){
        return reject(err);
      }
      else{
        return resolve(finded);
      }
    })
});
}
module.exports = service;

