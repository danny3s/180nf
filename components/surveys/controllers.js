const Services = require('./services');
const AuthService = require('../auth/services');

exports.newSurvey = (req,res,next) => {
	var id = req.user.sub;
	console.log(req.body);
	console.log(req.body.data);
	Services.newSurvey(req.body.data)
		.then(() =>{
			var data = {
				was_surveyed: true
			};
			AuthService.updateme(id, data)
			.then(updated =>{				
				res.status(200).send(updated);
			})
			.catch(errorupdated=>{
				res.status(500).send(errorupdated);
			})
		})
		.catch(error=>{
			res.status(500).send(error);
		})
}

exports.getAll = (req,res,next) => {
	Services.getAll()
	.then(resp =>{
		res.status(200).send(resp);
	})
	.catch(error=>{
		res.status(500).send(error);
	})
}
