const Services = require('./services'),
	request = require("request"),
	{ EMAIL_SERVICES, EMAIL_HOST } = process.env;


exports.register = function(req,res,next){
	console.log("AUTENTICANDO")
	console.log(EMAIL_SERVICES)

	var params = req.body;

   Services.register(params)
	.then(function(user){
	if (user.code==404) {
			res.send(user);
	}
	else{
		res.status(200).send({
			code:100,
			message:"Usuario creado con exito!",
			token:user.token,
			id: user.id,
			activationcode: user.activationcode
		});		
		}		
	})
	.catch(function(err){
		console.log("ERROR AL REGISTRAR")
		console.error(err);
		res.status(500).send(err)
	})
};
exports.login = function(req,res,next){
   Services.login(req.body)
	.then(function(result){

		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}


exports.setAvatar = function(req,res,next){
	var id = req.user.sub;
	console.log(id);
	var data = req;
	Services.setAvatar(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.me = function(req,res,next){
	var id = req.user.sub;
	console.log("USER ME!!!");
					console.log(id);
	Services.me(id)
	.then(function(result){
		console.log("RESPONSE!!!");
					console.log(result);
		res.send(result);
	})
	.catch(function(err){
		console.log("ERROR!!!");
					console.log(err);
		res.send(err);
	})
	
};
exports.meUltrasecret = function(req,res,next){
	const {params:{email}} = req;
	Services.meUltrasecret(email)
	.then(function(result){
		console.log("RESPONSE!!!");
					console.log(result);
		res.send(result);
	})
	.catch(function(err){
		console.log("ERROR!!!");
					console.log(err);
		res.send(err);
	})
	
};
exports.getMayWallet = function(req,res,next){
	var id = req.user.sub;
	Services.getMayWallet(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.updateme = function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	Services.updateme(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.forgot = function(req,res,next){
	var email = req.params.email;
	var type = req.params.type;
	Services.forgot(email,type)
	.then(function(result){
		console.log(result)
		var SendOptions = { method: 'POST',
					url: EMAIL_SERVICES,
					headers: 
					{ 	Accept: 'application/json',
						'Content-Type': 'application/json' },
					body: 
					{ email: result.userfinded.email,
						code: result.generatedcodes,
						template: 'recuperar_clave' },
					json: true };

			request(SendOptions, function (error, response, body) {
				if(error){
					res.status(500).send(error);
				}
				else{
					res.status(200).send({code:100,message:"Usuario actualizado con exito!"})
				}
			});	
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.setPin = function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	Services.setPin(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.newPin = function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	Services.newPin(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.setPass = function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	Services.setPass(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.activateme = function(req,res,next){
	var id = req.params.id;
	var data = req.body;
	Services.me(id)
	.then(function(finded){
		if(finded.activationcode === data.activationcode){
			let user = {
				isactive: true
			};
			 Services.updateme(id,user)
			 .then(function(updated){
				res.send(updated);
			})
			.catch(function(err){
				res.send(err);
			})
		}
		else{
		res.status(402).send({message:"Codigo de activación no valido"});

		}
	})
	.catch(function(err){
		res.send(err);
	})
	
};

exports.checkemail = function(req,res,next){
	var email = req.params.email;
	Services.checkemail(email)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getallCities = function(req,res,next){
	var country = req.params.country;
	var list = require('../../static/geo/'+country+'.json');
	res.status(200).send(list);
	
};
exports.activedUsersPayed = function(req,res,next){
	Services.activedUsersPayed()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};