'use strict'
const mongoose = require('mongoose');

const UserSchema =  new mongoose.Schema({
	username:{ type:String, required:true },
	activationcode: { type:String, required:true },
	email:{ type:String, required:true },
	password:{ type:String, required:true },
	avatar:{ type:String, required:false },
	isactive:{ type:Boolean,default:false },
	socialnetwork:{type:String},
	phone:{type:Number},
	city:{ type: String },
	state:{ type: String },
	country:{ type: String },
	register_date:{ type:Date  },
	is_social:{type:Boolean},
	wallet_address:{ type: String },
	wallet_key:{ type: String },
	score: {type:Number},
	was_surveyed: {type: Boolean, default: false},
	made_the_first_charge: {type: Boolean, default: false},
	pin: {type:String, default: "180"},
	products_registred: { type: Number }
});
module.exports.User = UserSchema;
  



