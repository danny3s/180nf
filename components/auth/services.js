const User = require('./model').User;
const service = {};
const moment = require('moment');
const bcrypt = require('bcrypt-nodejs');
const authservice = require('../../services/auth');
const randomstring = require("randomstring");
const connectToDatabase = require('../../config/db-connection');

service.startSuperAdmin = async (data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);

	return new Promise(function(resolve, reject){
   
      userModel.findOne({nombre:data.nombre},function(err,result){
        if(err){
          console.log('Error 1 -------------')
          return reject(err);
        }
        else if (result){
          console.log('Error 2 -------------')
          return resolve(result);
        }
        else{
          console.log('No existe -------------')
          userModel.create(data,function(err,UserCreado){
            if(err){
              console.log('Error 3 -------------')
              return reject(err);
            }
            else{
              console.log('Error 4 -------------')
              return	resolve(UserCreado);
            }
          })
        }
      })
   
	})
}
service.checkemail = async (data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);

  return new Promise(function(resolve, reject){
userModel.findOne({email:data.toLowerCase()}, function(err, user) {
        // Comprobar si hay errores
        // Si el User existe o no
        // Y si la contraseña es correcta
        if (err) {
            return reject({
                message: "Ocurrio un error al hacer la peticion",
                error:err
            });
        } else {
           if(user){
            if (!user.isactive) {
              return resolve({code:103,message:"La cuenta no esta activa"});
            }
            else{
              if (user.is_social) {
                return resolve({code:104,is_social:user.is_social});
              }
              else{
                return resolve({code:100,is_social:user.is_social});
              }
            }
           }
             else{
                    return reject({code:101,message:"El usuario no existe"});
                }
        }

    });
  })
}
service.register = async (params)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  var currentDate = moment().utc();
  var acticode = randomstring.generate(6);
   return new Promise(function(resolve, reject){
      var user = new userModel({
        username : params.username,
        email: params.email,
        password:params.password,
        id_type:params.id_type,
        id_num:params.id_num,
        city:params.city,
        lat:params.lat,
        long:params.long,
        state:params.state,
        country:params.country,
        register_date:currentDate,
        is_social:params.is_social,
        activationcode: acticode
    });
    if (!params.avatar) {
    user.avatar="-";
    }
    else{
    user.avatar=params.avatar;
    }
    bcrypt.hash(params.password,null,null,function(err,hash){
      if(err){
          console.log(err);
      }
      else{
          user.password = hash;
      }
    })
    bcrypt.hash(params.pin,null,null,function(err,hash){
      if(err){
          console.log(err);
      }
      else{
          user.pin = hash;
      }
    })
    
    userModel.findOne({email:params.email},(err,userfinded)=>{
    if (userfinded) {
      return resolve({code:404,message:"Este usuario ya existe"});
    }
    else{
        userModel.create(user,function(err,usercreated) {
      if (err) {
          return reject({code:501,data:{message:"Error al crear el usuario", err: err}});
      }
      else{
        var token = authservice.createToken(usercreated);
      
          return resolve({code:100,message:"Usuario creado con exito!",id: usercreated._id, email:params.email,activationcode: acticode, token:token});
      }
    })
    }
    })
   });

}
service.login = async (data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
	return new Promise(function(resolve, reject){
  userModel.findOne({email:data.email.toLowerCase()}, function(err, user) {
        if (err) {
            return reject({
                message: "Petición denegada, correo o contraseña invalidos"
            });
        } else {
           if(user){
            if(data.loginwithpin){
             bcrypt.compare(data.pin,user.pin,(err,check)=>{
                if(check){
                        return resolve({code:100,token:authservice.createToken(user)});
                }
                else{
                  console.log("PIN", err);
                   return reject({code:404,message:"El usuario no pudo logearse correctamente"});
                }
            })
            }
            else{
             bcrypt.compare(data.password,user.password,(errPassword,check)=>{
                if(check){
                        return resolve({code:100,token:authservice.createToken(user)});
                }
                else{
                  console.log("PIN", errPassword);

                   return reject({code:404,message:"El usuario no pudo logearse correctamente"});
                }
            })
            }
           }
             else{
                    return reject({code:404,message:"El usuario no pudo logearse correctamente"});
                }
        }

    });
	})
}

service.setAvatar = async (id,data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
    return new Promise(function(resolve, reject){
        
         if (!data.files){
                return reject({message:"No hay imagenes"});
         }
         else{
            // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
              let avatar = data.files.avatar;
             console.log(id);
              // Use the mv() method to place the file somewhere on your server
              avatar.mv('./uploads/avatars/'+id+'.jpg', function(err,res) {
                    if(err){
                        return reject(err);
                    }
                    else {
                        var localdata = {
                            avatar: process.env.IMAGESHOST+'/avatars/'+id+'.jpg'
                        }
                        userModel.findByIdAndUpdate(id,localdata,{new:true},function(error,result){
                        if(err){
                            return reject(error);
                        }
                        else if (result){
                            return resolve(result);
                        }
                    })
                    }
              });
         }

    })
}


service.me = async (id)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
    return new Promise(function(resolve, reject){
    userModel.findById(id, function(err, user) {
        if (err) {
            console.log("USER ME SERVICE ERROR!!!");
            console.error(err);
             return reject(err);
        }
        else{
          if (user) {
            console.log("USER ME SERVICE!!!");
					  console.log(user);
            return resolve(user);
          }
            else{
              console.log("USER ME SERVICE ERROR!!!");
					    console.error("el usuario no existe");
              return reject({message:"el usuario no existe",code:404});
            }
        }
    });
    })
}

service.meUltrasecret = async (email)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
    return new Promise(function(resolve, reject){
    userModel.findOne({email})
    .select("-password -activationcode -pin")
    .exec(function(err, user) {
        if (err) {
             return reject(err);
        }
        else{
          if (user) {
            return resolve(user);
          }
            else{
              return reject({message:"el usuario no existe",code:404});
            }
        }
    });
    })
}
service.getMayWallet = async (id)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  return new Promise(function(resolve, reject){
  userModel.findById(id, function(err, list) {
      if (err) {
           return reject(err);
      }
      else{
        if (list) {

          return resolve(list);
        }
          else{
            return reject({message:"el usuario no existe",code:404});
          }
      }
  });
  })
} 
service.updateme =  async (id,data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
    return new Promise(function(resolve, reject){
    userModel.findByIdAndUpdate(id,data,{new:true}, function(err, updated) {
        if (err) {
             return reject(err);
        }
        else{
          var token = authservice.createToken(updated);   
          return resolve({code:100,message:"User actualizado con exito!",token:token,updated:updated});
        }
    });
    })
}

service.forgot =  async (email, type)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  return new Promise(function(resolve, reject){
    userModel.findOne({email},(err,userfinded)=>{
      if(err){
        return reject(err);
      }
      else{
        console.log(userfinded);
        if(userfinded == null){
          return reject({message:"Usuario no encontrado", status: 404})
        }else{
          var generatedcodes = type == "pin"? randomstring.generate({
            length: 4,
            charset: 'numeric'
          }): randomstring.generate(6);
            bcrypt.hash(generatedcodes,null,null,function(err,hash){
              if(err){
                  console.log(err);
              }
              else{
                if(type=="pin"){
                  userfinded.pin = hash;
                }
                else{
                userfinded.password = hash;
                }

                console.log("_____________-")
                console.log(userfinded)
                console.log(userfinded._id);
              userModel.findByIdAndUpdate(userfinded._id, userfinded, {new:true}, function(errUpdate, updated){
                if(errUpdate) {
                  return reject(errUpdate);
                }
                else{
                  console.log("________________--")
                  console.log(updated);
                  return resolve({userfinded, generatedcodes});
                }
              })
              }
            })
          
        }
      }
    });
  })
}

service.setPin =  async (id,data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  return new Promise(function(resolve, reject){
    userModel.findById(id,function(err,usr){
      bcrypt.compare(data.pin,usr.pin,(err,check)=>{
              if(check){
                    var pass = data.newpin;
                    var user = data;
              bcrypt.hash(pass,null,null,function(err,hash){
                  if(err){
                      console.log(err)
                  }
                  else{
                    user.pin = hash;
                      userModel.findByIdAndUpdate(id,user,{new:true}, function(err, list) {
                          if (err) {
                              return reject(err);
                          }
                          else{
                              return resolve(list);
                          }
                      });
                  }
              });
              }
              else{
                console.log(err);
                 return reject({code:404,message:"Esta no es tu contraseña"});
              }
          })
    })
      
  })
}


service.newPin =  async (id,data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  return new Promise(function(resolve, reject){
    bcrypt.hash(data.newpin,null,null,function(err,hash){
      if(err){
          console.log(err)
      }
      else{
          data.pin = hash;
          console.log(data);
          userModel.findByIdAndUpdate(id,data,{new:true}, function(err, list) {
              if (err) {
                  return reject(err);
              }
              else{
                  return resolve(list);
              }
          });
      }
    });
  })
}

service.setPass =  async (id,data)=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  return new Promise(function(resolve, reject){
    userModel.findById(id,function(err,usr){
      bcrypt.compare(data.pass,usr.password,(err,check)=>{
              if(check){
                    var pass = data.newpass;
                    var user = data;
              bcrypt.hash(pass,null,null,function(err,hash){
                  if(err){
                      console.log(err)
                  }
                  else{
                      user.password = hash;
                      console.log(user);
                      userModel.findByIdAndUpdate(id,user,{new:true}, function(err, list) {
                          if (err) {
                              return reject(err);
                          }
                          else{
                              return resolve(list);
                          }
                      });
                  }
              });
              }
              else{
                console.log(err);
                 return reject({code:404,message:"Esta no es tu contraseña"});
              }
          })
    })
      
  })
}


service.activedUsersPayed = async ()=> {
  let connection = await connectToDatabase();
  let userModel = connection.model('User',User);
  return new Promise(function(resolve, reject){
    var queryFinder = userModel.find({made_the_first_charge:true}).select('wallet_address');

    queryFinder.exec(function (err, users) {
      if (err) {
        return reject(err);
      }
      else{
        if (users) {
          return resolve(users);
        }
          else{
            return reject({message:"No hay usuarios activados",code:404});
          }
      }
    });
  })
}
module.exports = service;

