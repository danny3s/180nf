var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
const fileUpload = require('express-fileupload');
//Inicializando el objeto express
var app = express();
var subdomain = require('express-subdomain');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.engine('html', require('hbs').__express);
app.set('views',path.join(__dirname,'./views'));
app.set('view engine', 'hbs');
var auth = require('./components/auth/rutes');

//CORS ORIGIN MIDELWARE
app.use((req,res,next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','X-API-KEY, Origin, X-Requested-Width, Content-Type, Accept,Access-Control-Request-Method,Authorization');
	res.header('Access-Control-Request-Methods','GET, POST,OPTIONS,PUT,DELETE');
	res.header('Allow','GET, POST,OPTIONS,PUT,DELETE');
	next();
})


app.use(fileUpload());
app.use(subdomain('docs', express.static(path.join(__dirname, '../backend/apidoc/'))));
app.use(subdomain('files', express.static(path.join(__dirname, '../backend/uploads/'))));
// var routes = app.use('/api/v1',auth);
// app.use(subdomain('v1.api', routes));
app.use('/api/v1',auth);
module.exports = app;
