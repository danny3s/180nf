'use strict'
const express = require('express'),
	  serverless = require('serverless-http'),	
	  AuthController = require('./components/auth/controllers'),
	  SurveysController = require('./components/surveys/controllers'),
	  SubscriptorsController = require('./components/subscriptions/controllers'),
	  ScannerController = require('./components/scanner/controllers'),
	  userMiddleware = require('./middlewares/auth'),
	  cors = require("cors"),
	  bodyParser = require("body-parser"),
	  app = express();
	  app.use(cors());
	  // parse application/x-www-form-urlencoded
	  app.use(bodyParser.urlencoded({ extended: false }));

	  // parse application/json
	  app.use(bodyParser.json());
    

	app.get('/api/v1/auth/checkemail/:email',AuthController.checkemail);
	app.post('/api/v1/auth/register',AuthController.register);
	app.post('/api/v1/auth/login',AuthController.login);
	app.get('/api/v1/auth/me',userMiddleware.isAuth,AuthController.me);
	app.get('/api/v1/auth/me-ultra-secret/:email',AuthController.meUltrasecret);
	app.get('/api/v1/users/actived/users-payed',AuthController.activedUsersPayed);
	app.get('/api/v1/auth/get-my-wallet',userMiddleware.isAuth,AuthController.getMayWallet);
	app.post('/api/v1/auth/update-me',userMiddleware.isAuth,AuthController.updateme);
	app.post('/api/v1/auth/set-pin',userMiddleware.isAuth,AuthController.setPin);
	app.post('/api/v1/auth/set-pass',userMiddleware.isAuth,AuthController.setPass);
	app.post('/api/v1/auth/new-pin',userMiddleware.isAuth,AuthController.newPin);
	app.post('/api/v1/auth/activate-me/:id',AuthController.activateme);
	app.post('/api/v1/user/set-avatar',userMiddleware.isAuth,AuthController.setAvatar);
	app.get('/api/v1/geo/all-cities/:country',AuthController.getallCities);
	app.get('/api/v1/auth/forgot/:type/:email',AuthController.forgot);
	app.post('/api/v1/subscriptors/subscribe',SubscriptorsController.subscribe);
	app.get('/api/v1/subscriptors/get-all-subscriptors',SubscriptorsController.allsubscriptors);
	app.post('/api/v1/surveys/new-survey',userMiddleware.isAuth,SurveysController.newSurvey);
	app.get('/api/v1/surveys/get-all',SurveysController.getAll);
	app.get('/api/v1/scanner/check-for-product/:product_code',ScannerController.checkForProduct);
	app.post('/api/v1/scanner/new-product',userMiddleware.isAuth,ScannerController.NewProduct);
	app.get('/api/v1/scanner/get-products-globally/:perPage/:page',ScannerController.getProducts);  
	app.get('/api/v1/scanner/get-my-scann-products/:perPage/:page',userMiddleware.isAuth,ScannerController.getMyParticipations);  
	app.get('/api/v1/scanner/products-verified/:perPage/:page',ScannerController.getVerifiedProducts);  
	app.post('/api/v1/scanner/update-product/:id', ScannerController.updateProduct);
	app.get('/api/v1/scanner/get-product-by-code/:code', ScannerController.getProductByCode);
module.exports.handler = serverless(app);
