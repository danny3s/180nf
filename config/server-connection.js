module.exports = {  
    ssl_port: process.env.SSLPORT,
    non_ssl_port: process.env.NONSSLPORT,
    enviroment: process.env.ENVIROMENT,
    ssl_connection:{
      key: process.env.CERTKEY,
      cert: process.env.DOMAINKEY,
      ca1: process.env.CA1,
      ca2: process.env.CA2,
      ca3: process.env.CA3
    }
  };